package de.lvm.javaakademie.streams;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		final Stream<String> lines = new BufferedReader(new FileReader("/home/m500092/Schreibtisch/Test.txt")).lines();
		lines.flatMap((in) -> Stream.of(in.split(" ")))
				.filter(w -> !w.matches(".*[äÄöÖüÜ].*")).map(in -> in.replace(',', ' ').trim())
				.collect(Collectors.toMap(Function.identity(), (in) -> 1, (one, two) -> one + two))
				.forEach((key, value) -> System.out.println(key + ":" + value));
	}
}
