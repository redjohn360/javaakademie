package de.lvm.javaakademie.teil6.uebung2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class MinMax {

	private LinkedList<Integer> list;
	private Random random;


	public MinMax() {
		random = new Random();
	}

	public void erzeugeUndFuelleListe (final int anzahl) {
		list = new LinkedList<Integer>();
		for (int i = 0; i < anzahl; i++) {
			list.add(random.nextInt());
		}
	}

	public void ermittleMaxMitIterrator () {
		int max = 0;
		long start = System.currentTimeMillis();
		for (int current: list) {
			if (current > max) {
				max = current;
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("Maximum ist " + max);
		System.out.println("Zeit: " + (end-start));
	}

	public void ermittleMaxMitIndex () {
		int max = 0;
		long start = System.currentTimeMillis();
		for (int i = 0; i < list.size(); i++) {
			int current = list.get(i);
			if (current > max) {
				max = current;
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("Maximum ist " + max);
		System.out.println("Zeit: " + (end-start));
	}
}
