package de.lvm.javaakademie.teil6.uebung1;

public class Pair <Key extends Comparable<Key>, Wert> implements Comparable<Pair<Key, Wert>> {
	private final Key key;
	private final Wert wert;

	public Pair(final Key key, final Wert wert) {
		this.key = key;
		this.wert = wert;
	}

	public Key getKey() {
		return key;
	}

	public Wert getWert() {
		return wert;
	}

	@Override
	public int compareTo(final Pair<Key, Wert> o) {
		return o.getKey().compareTo(key);
	}
}
