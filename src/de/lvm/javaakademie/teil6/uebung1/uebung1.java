package de.lvm.javaakademie.teil6.uebung1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class uebung1 {

	public static void main (String[] args) {
		List<Pair<String, Person>> liste = new ArrayList<>();
		final Person person = new Person("Gert");
		final Person person1 = new Person("Hans");

		liste.add(new Pair("wadkljsadiwq32132", person));
		liste.add(new Pair("dw92iw�j92jd02", person1));
		liste.add(new Pair("d09jsoipoijddw92iw�j92jd02", person1));
		liste.add(new Pair("dw92iw�j9w09q123e2jd02", person1));
		liste.add(new Pair("dw92iw�j92jdfihewoizqwd02", person1));
		liste.add(new Pair("dw92iw�j92jdw098hqojsa02", person1));
		liste.add(new Pair("dw92isah9qzuwdw�j92jd02", person1));
		liste.add(new Pair("dw92iw�oiduwqoihjqwbj92jd02", person1));
		liste.add(new Pair("dw2qokjdaq92iw�j92jd02", person1));
		liste.add(new Pair("dw9209q09821oijdiw�j92jd02", person1));

		Collections.sort(liste);
		Collections.reverse(liste);

		for (Pair pair: liste) {
			System.out.println("[" + pair.getWert() + ": " + pair.getKey() + "]");
		}
	}
}
