package de.lvm.javaakademie.teil6.uebung1;

public class Person {
	private final String name;

	public Person(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
