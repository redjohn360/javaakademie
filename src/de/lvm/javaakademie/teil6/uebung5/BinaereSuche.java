package de.lvm.javaakademie.teil6.uebung5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import utils.timer;

public class BinaereSuche {

	public static void main(String[] args) {
		BinaereSuche b = new BinaereSuche();
	}

	ArrayList<Vertrag> liste = new ArrayList<>();

	public BinaereSuche() {
		Random r = new Random();

		for (int i = 0; i < 500000; i++) {
			liste.add(new Vertrag(i, i));
		}
		System.out.print("Sortieren - ");
		timer.start();
		Collections.sort(liste);
		timer.stop();

		timer.start();
		for (int i = 0; i < 5000; i++) {
			liste.indexOf(new Vertrag(r.nextInt(500000), 0));
		}
		timer.stop();

		timer.start();
		for (int i = 0; i < 5000; i++) {
			Collections.binarySearch(liste, new Vertrag(r.nextInt(500000), 0));
		}
		timer.stop();
	}
}
