package de.lvm.javaakademie.teil6.uebung5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class HashMapUebung {

	private final HashMap<Integer, Vertrag> hashMap = new HashMap<>();
	private final ArrayList<Vertrag> liste = new ArrayList<>();
	private final Random r = new Random();

	public void fuelleListeUndHashMapMitVertreagen(final int i) {
		for (int y = 0; y < i; y++) {
			final Vertrag vertrag = new Vertrag(r.nextInt(1000000000), r.nextInt(1000000000));
			hashMap.put(vertrag.getVsnr(), vertrag);
			liste.add(vertrag);
		}
	}

	public Vertrag findeVertragListe(final int vsnr) {
		for (Vertrag vertrag: liste) {
			if (vertrag.getVsnr() == vsnr) {
				return vertrag;
			}
		}
		return null;
	}

	public Vertrag findeVeragHas(final int vnsr) {
		return hashMap.get(vnsr);
	}
}
