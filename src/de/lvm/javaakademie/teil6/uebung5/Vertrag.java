package de.lvm.javaakademie.teil6.uebung5;

import java.util.Objects;

public class Vertrag implements Comparable<Vertrag> {

	private int vsnr;
	private int vertragsnummer;

	public Vertrag(final int vsnr, final int vertragsnummer) {
		this.vsnr = vsnr;
		this.vertragsnummer = vertragsnummer;
	}

	public int getVsnr() {
		return vsnr;
	}

	public void setVsnr(final int vsnr) {
		this.vsnr = vsnr;
	}

	public int getVertragsnummer() {
		return vertragsnummer;
	}

	public void setVertragsnummer(final int vertragsnummer) {
		this.vertragsnummer = vertragsnummer;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		final Vertrag vertrag = (Vertrag) o;
		return vsnr == vertrag.vsnr;
	}

	@Override
	public int hashCode() {
		return Objects.hash(vsnr, vertragsnummer);
	}

	@Override
	public int compareTo(final Vertrag o) {
		Vertrag anderer = (Vertrag) o;
		if (anderer.getVsnr() > vsnr) {
			return -1;
		} else if (anderer.getVsnr() < vsnr) {
			return 1;
		}
		return 0;
	}
}
