package de.lvm.javaakademie.teil6.uebung3;

import java.util.ArrayList;

public class uebung3 {
	public static void main (String[] args) {
		final Vertrag vertrag1 = new Vertrag(123, 123);
		final Vertrag vertrag2 = new Vertrag(1234, 1234);
		final Vertrag vertrag3 = new Vertrag(12345, 12345);
		final Vertrag vertrag4 = new Vertrag(123456, 123456);
		final Vertrag vertrag5 = new Vertrag(1234567, 1234567);
		final Vertrag vertrag6 = new Vertrag(1234567, 1234567);

		final ArrayList<Vertrag> liste = new ArrayList<>();
		liste.add(vertrag1);
		liste.add(vertrag2);
		liste.add(vertrag3);
		liste.add(vertrag4);
		liste.add(vertrag5);

		System.out.println(liste.contains(vertrag1));
		System.out.println(liste.indexOf(vertrag6));
	}
}
