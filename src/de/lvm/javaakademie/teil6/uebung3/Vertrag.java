package de.lvm.javaakademie.teil6.uebung3;

public class Vertrag {

	private int vertragsnummer;
	private int versicherungsnumer;

	public Vertrag(final int vertragsnummer, final int versicherungsnumer) {
		this.vertragsnummer = vertragsnummer;
		this.versicherungsnumer = versicherungsnumer;
	}

	public int getVertragsnummer() {
		return vertragsnummer;
	}

	public void setVertragsnummer(final int vertragsnummer) {
		this.vertragsnummer = vertragsnummer;
	}

	public int getVersicherungsnumer() {
		return versicherungsnumer;
	}

	public void setVersicherungsnumer(final int versicherungsnumer) {
		this.versicherungsnumer = versicherungsnumer;
	}

	@Override
	public boolean equals(final Object obj) {
		return ((Vertrag)obj).getVertragsnummer() == vertragsnummer;
	}
}
