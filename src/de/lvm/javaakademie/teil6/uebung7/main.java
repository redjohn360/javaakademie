package de.lvm.javaakademie.teil6.uebung7;

import java.math.BigInteger;

import utils.timer;

public class main {

	public static void main(String[] args) {
		timer.start();
		for (int i = 40; i <= 70; i++) {
			System.out.println(computeFaculty(i));
		}
		timer.stop();

		timer.start();
		BigInteger[] res = computeFacultyInRange(40, 70);
		for (BigInteger bin: res) {
			System.out.println(bin);
		}
		timer.stop();
	}

	public static BigInteger computeFaculty(int eingabe) {
		BigInteger result = new BigInteger("1");
		while (eingabe > 0) {
			result = result.multiply(new BigInteger(String.valueOf(eingabe)));
			eingabe--;
		}
		return result;
	}

	public static BigInteger[] computeFacultyInRange(int anf,int end) {
		BigInteger[] res = new BigInteger[end-anf];
		res[0] = computeFaculty(anf);
		for (int i = 1; i < end-anf; i++) {
			res[i] = new BigInteger(String.valueOf(anf + i)).multiply(res[i-1]);
		}
		return res;
	}
}
