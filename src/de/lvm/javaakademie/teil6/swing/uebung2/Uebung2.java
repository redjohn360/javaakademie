package de.lvm.javaakademie.teil6.swing.uebung2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Uebung2 {

	public static void main(String[] args) {
		JFrame frame = new MyFrame();
		frame.setForeground(Color.green.brighter());
		frame.setBackground(Color.green.darker());
		frame.setSize(200, 200);
		frame.setVisible(true);
		frame.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					frame.setVisible(false);
					frame.dispose();
					System.exit(0);
				}
			}
		});
	}


	static class MyFrame extends JFrame {
		@Override
		public void paint(Graphics g) {

		}
	}
}
