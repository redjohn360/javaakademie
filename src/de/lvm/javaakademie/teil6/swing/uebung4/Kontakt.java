package de.lvm.javaakademie.teil6.swing.uebung4;

public class Kontakt {
	private int kontaktnummer;
	private String name;
	private String vorname;
	private String telefonnummer;
	private String email;
	private String beschreibung;

	public Kontakt(final int kontaktnummer, final String name, final String vorname, final String telefonnummer,
			final String email,
			final String beschreibung) {
		this.kontaktnummer = kontaktnummer;
		this.name = name;
		this.vorname = vorname;
		this.telefonnummer = telefonnummer;
		this.email = email;
		this.beschreibung = beschreibung;
	}

	public int getKontaktnummer() {
		return kontaktnummer;
	}

	public String getName() {
		return name;
	}

	public String getVorname() {
		return vorname;
	}

	public String getTelefonnummer() {
		return telefonnummer;
	}

	public String getEmail() {
		return email;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setKontaktnummer(final int kontaktnummer) {
		this.kontaktnummer = kontaktnummer;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setVorname(final String vorname) {
		this.vorname = vorname;
	}

	public void setTelefonnummer(final String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public void setBeschreibung(final String beschreibung) {
		this.beschreibung = beschreibung;
	}
}
