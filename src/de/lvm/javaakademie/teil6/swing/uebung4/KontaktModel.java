package de.lvm.javaakademie.teil6.swing.uebung4;

import java.util.ArrayList;

public class KontaktModel {

	ArrayList<Kontakt> kontaktListe = new ArrayList<Kontakt>();
	private int kontaktnummerZaehler = 1100;

	public void anlegen(String name, String vorname, String telefonnummer, String email, String beschreibung,
			boolean beschreibungCkecked) {
		if(!beschreibungCkecked) {
			beschreibung = null;
		}
		Kontakt kontaktneu = new Kontakt(kontaktnummerZaehler, name, vorname, telefonnummer, email, beschreibung);
		kontaktListe.add(kontaktneu);
		kontaktnummerZaehler++;
	}

	public Kontakt anzeigen(String kontaktnummer) {
		if(kontaktnummer.equals("")) {
			return null;
		}
		int kontaktnummerNeu = Integer.parseInt(kontaktnummer);

		for (Kontakt k : kontaktListe) {
			if (k.getKontaktnummer() == kontaktnummerNeu) {
				return k;
			}
		}
		return null;
	}
}
