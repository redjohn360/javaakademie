package de.lvm.javaakademie.teil6.swing.uebung4;

import java.awt.*;

import javax.swing.*;

public class KontakView {

	private JFrame frame = new JFrame();

	private JButton anzeigen = new JButton("Anzeigen");

	private JButton anlegen = new JButton("Anlegen");

	private JTextField feldKontaktnummer = new JTextField();

	private JTextField feldName = new JTextField();

	private JTextField feldVorname = new JTextField();

	private JTextField feldTelefonnummer = new JTextField();

	private JTextField feldEmail = new JTextField();

	private JTextArea beschreibung = new JTextArea();

	private JCheckBox beschreibungUebernehmen = new JCheckBox("Beschreibung übernehmen");

	private JTextField geburtsDatum = new JTextField();


	public KontakView() {
		super();

		frame.setSize(500, 300);
		GridLayout layout = new GridLayout(0, 2);
		frame.setLayout(layout);
		JLabel kontaktnummer = new JLabel();
		kontaktnummer.setText("Kontaktnr.");
		JLabel name = new JLabel();
		name.setText("Name");
		JLabel vorname = new JLabel();
		vorname.setText("Vorname");
		JLabel telefonnummer = new JLabel();
		telefonnummer.setText("Telefonnr.");
		JLabel email = new JLabel();
		email.setText("Email");
		JLabel jlBeschreibung = new JLabel();
		jlBeschreibung.setText("Beschreibung");

		JScrollPane scrollPane = new JScrollPane(beschreibung);

		frame.add(kontaktnummer);
		frame.add(feldKontaktnummer);
		frame.add(name);
		frame.add(feldName);
		frame.add(vorname);
		frame.add(feldVorname);

		frame.add(new JLabel("Geburtsdatum"));
		frame.add(geburtsDatum);

		frame.add(telefonnummer);
		frame.add(feldTelefonnummer);
		frame.add(email);
		frame.add(feldEmail);
		frame.add(jlBeschreibung);
		frame.add(scrollPane);
		frame.add(new JPanel());
		frame.add(beschreibungUebernehmen);
		frame.add(anzeigen);
		frame.add(anlegen);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

	}

	public JButton getAnzeigen() {
		return anzeigen;
	}

	public JButton getAnlegen() {
		return anlegen;
	}

	public JTextField getFeldKontaktnummer() {
		return feldKontaktnummer;
	}

	public JTextField getFeldName() {
		return feldName;
	}

	public JTextField getFeldVorname() {
		return feldVorname;
	}

	public JTextField getFeldTelefonnummer() {
		return feldTelefonnummer;
	}

	public JTextField getFeldEmail() {
		return feldEmail;
	}

	public JTextArea getBeschreibung() {
		return beschreibung;
	}

	public JCheckBox getBeschreibungUebernehmen() {
		return beschreibungUebernehmen;
	}
}
