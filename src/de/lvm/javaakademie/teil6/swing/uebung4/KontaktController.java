package de.lvm.javaakademie.teil6.swing.uebung4;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class KontaktController {

	public static void main(String[] args) {
		KontaktController controller = new KontaktController();
		controller.start();
	}

	private KontakView view;
	private KontaktModel aktion;

	public void start() {
		view = new KontakView();
		aktion = new KontaktModel();
		addListener();
	}

	public void addKonakt(final JTextField feldKontaktnummer, final JTextField feldName,
	final JTextField feldVorname, final JTextField feldTelefonnummer, final JTextField feldEmail) {

		if (feldName.getText().isEmpty() || feldVorname.getText().isEmpty()
				|| feldTelefonnummer.getText().isEmpty() || feldEmail.getText().isEmpty()) {
			feldName.setBackground(Color.RED);
			feldVorname.setBackground(Color.RED);
			feldTelefonnummer.setBackground(Color.RED);
			feldEmail.setBackground(Color.RED);
		} else {
			aktion.anlegen(feldName.getText(), feldVorname.getText(), feldTelefonnummer.getText(),
					feldEmail.getText(), view.getBeschreibung().getText(), view.getBeschreibungUebernehmen().isSelected());
			feldKontaktnummer
					.setText("" + aktion.kontaktListe.get(aktion.kontaktListe.size() - 1).getKontaktnummer());
			feldKontaktnummer.setBackground(Color.GREEN);
		}
	}

	public void finden(final JTextField feldKontaktnummer, final JTextField feldName,
			final JTextField feldVorname, final JTextField feldTelefonnummer, final JTextField feldEmail) {

		Kontakt anzeige = aktion.anzeigen(feldKontaktnummer.getText());
		if (anzeige == null) {
			feldKontaktnummer.setBackground(Color.RED);
			feldName.setText("Diese Nummer");
			feldVorname.setText("ist nicht");
			feldTelefonnummer.setText("vergeben");
		} else {
			feldName.setText(anzeige.getName());
			feldVorname.setText(anzeige.getVorname());
			feldTelefonnummer.setText(anzeige.getTelefonnummer());
			feldEmail.setText(anzeige.getEmail());
			view.getBeschreibung().setText(anzeige.getBeschreibung());
		}
	}

	public void addListener() {
		view.getAnzeigen().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				view.getFeldKontaktnummer().setBackground(Color.WHITE);
				view.getFeldName().setBackground(Color.WHITE);
				view.getFeldVorname().setBackground(Color.WHITE);
				view.getFeldTelefonnummer().setBackground(Color.WHITE);
				view.getFeldEmail().setBackground(Color.WHITE);

				view.getFeldName().setText("");
				view.getFeldVorname().setText("");
				view.getFeldTelefonnummer().setText("");
				view.getFeldEmail().setText("");
				view.getBeschreibung().setText("");

				finden(view.getFeldKontaktnummer(), view.getFeldName(), view.getFeldVorname(),
						view.getFeldTelefonnummer(), view.getFeldEmail());
			}
		});

		view.getAnlegen().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				view.getFeldKontaktnummer().setBackground(Color.WHITE);
				view.getFeldName().setBackground(Color.WHITE);
				view.getFeldVorname().setBackground(Color.WHITE);
				view.getFeldTelefonnummer().setBackground(Color.WHITE);
				view.getFeldEmail().setBackground(Color.WHITE);

				addKonakt(view.getFeldKontaktnummer(), view.getFeldName(), view.getFeldVorname(),
						view.getFeldTelefonnummer(), view.getFeldEmail());
			}
		});
	}
}