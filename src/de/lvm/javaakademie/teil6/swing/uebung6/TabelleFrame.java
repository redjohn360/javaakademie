package de.lvm.javaakademie.teil6.swing.uebung6;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class TabelleFrame extends JFrame {

    private final JTable tabelle;


    public TabelleFrame(final String[][] data, final String[] headers) {
        super("Kontakte - Übersicht");
        setLayout(new BorderLayout());

        tabelle = new JTable(data, headers)
        {
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
            {
            	Component c = super.prepareRenderer(renderer, row, column);

							c.setBackground(getBackground());
							int modelRow = convertRowIndexToModel(row);
							String type = (String) getModel().getValueAt(modelRow, 3);

							DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
							LocalDate date;

							date = LocalDate.parse(type, dateTimeFormatter);
							date = LocalDate.ofYearDay(LocalDate.now().getYear(), date.getDayOfYear());


							if (date.isBefore(LocalDate.now())) {
								c.setBackground(Color.RED);
							} else if(date.isAfter(LocalDate.now())) {
								final int diff = date.getDayOfYear() - LocalDate.now().getDayOfYear();
								if(diff < 14) {
										c.setBackground(Color.GREEN);
								} else {
										c.setBackground(Color.YELLOW);
								}
							}
							return c;
            }
        };
        tabelle.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

        add(new JScrollPane(tabelle), BorderLayout.CENTER);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(500,300);
        setVisible(true);
    }

    public TableModel getTableModel() {
        return tabelle.getModel();
    }
}
