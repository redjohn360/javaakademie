package de.lvm.javaakademie.teil6.swing.uebung6;

import de.lvm.javaakademie.teil6.swing.uebung6.events.FormEvent;

public class Kontakt {
    private int kontaktnummer;
    private String name;
    private String vorname;
    private String geburtsdatum;
    private String telefonnummer;
    private String email;
    private String beschreibung;

    public Kontakt(int kontaktnummer, String name, String vorname, String geburtsdatum, String telefonnummer, String email, String beschreibung) {
        this.kontaktnummer = kontaktnummer;
        this.name = name;
        this.vorname = vorname;
        this.geburtsdatum = geburtsdatum;
        this.telefonnummer = telefonnummer;
        this.email = email;
        this.beschreibung = beschreibung;
    }

    public Kontakt(FormEvent event, int kontaktnummer) {
        this.kontaktnummer = kontaktnummer;
        this.name = event.getName();
        this.vorname = event.getVorname();
        this.geburtsdatum = event.getGeburtsdatum();
        this.telefonnummer = event.getTelefonnummer();
        this.email = event.getEmail();
        this.beschreibung = event.isBeschreibungUebernehmen() ? event.getBeschreibung() : "";
    }

    public int getKontaktnummer() {
        return kontaktnummer;
    }

    public void setKontaktnummer(int kontaktnummer) {
        this.kontaktnummer = kontaktnummer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(String geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }
}
