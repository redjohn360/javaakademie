package de.lvm.javaakademie.teil6.swing.uebung6;

import javax.swing.*;

public class AppStart {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            MainFrame view = new MainFrame();
        });
    }
}
