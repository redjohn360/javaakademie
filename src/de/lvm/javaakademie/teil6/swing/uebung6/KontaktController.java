package de.lvm.javaakademie.teil6.swing.uebung6;


import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import de.lvm.javaakademie.teil6.swing.uebung6.events.FormEvent;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class KontaktController {

    private final String SAVE_FILE_LOCATION = getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "save.json";

    private final MainFrame mainFrame;
    private List<Kontakt> kontakte;
    private final Gson gson;

    public KontaktController(final MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        kontakte = new ArrayList<>();
        gson = new Gson();

        loadFromJson();

        mainFrame.getFormPanel().setFormListener(event -> {
            if(event.isNeuAnlegen()){
                addKontakt(event);
            } else {
                final Kontakt kontakt = searchKontakt(event);
                if(kontakt == null) {
                    displayError("Der Kontakt mit der Nummer " + event.getKontaktnummer() + " konnte nicht gefunden werden.");
                    return;
                }
                mainFrame.getFormPanel().displayKontakt(kontakt);
            }
        });

        mainFrame.getFormPanel().setButtonListener(() -> {
            String[] headers = {"Kontaktnr.", "Name", "Vorname", "Geburtsdatum", "Telefonnr.", "E-Mail"};

            TabelleFrame tabelle = new TabelleFrame(getTableCells(), headers);
        });

        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                saveToFile();
                e.getWindow().dispose();
            }
        });
    }

    private void addKontakt(FormEvent event) {

            if (event.getName().isEmpty() || event.getVorname().isEmpty() || event.getEmail().isEmpty() ||
                    event.getTelefonnummer().isEmpty() || event.getGeburtsdatum().isEmpty()) {
                displayError("Es müssen alle Felder ausgefüllt wertden!");
                return;
            }

            if (!event.getGeburtsdatum().matches("((0[1-9]|[12]\\d|3[01])\\.(0[1-9]|1[0-2])\\.[12]\\d{3})")) {
                displayError("Das Geburtsdatum ist nicht gültig.");
                return;
            }

            final Kontakt kontakt = new Kontakt(event, generateKontaktnummer());
            kontakte.add(kontakt);
            mainFrame.getFormPanel().displayKontakt(kontakt);
    }

    private void displayError(final String message) {
        JOptionPane.showMessageDialog(null, message, "Fehler", JOptionPane.WARNING_MESSAGE);
    }

    private Kontakt searchKontakt(FormEvent event) {
        if (event.getKontaktnummer().isEmpty()) {
            return null;
        }
        try {
            int kontaktnummer = Integer.parseInt(event.getKontaktnummer());
            for(Kontakt kontakt: kontakte) {
                if (kontakt.getKontaktnummer() == kontaktnummer) {
                    return kontakt;
                }
            }
        } catch (NumberFormatException e) {
            displayError("Die Kontaktnummer ist keine gültige Nummer.");
        }
        return null;
    }

    private int generateKontaktnummer() {
        if(kontakte.isEmpty()){
            return 1;
        } else {
            return kontakte.get(kontakte.size()-1).getKontaktnummer() + 1;
        }
    }

    private void saveToFile(){
        final KontaktListe kontaktListe = new KontaktListe(kontakte);
        final String json = gson.toJson(kontaktListe);

        try (PrintWriter writer = new PrintWriter(SAVE_FILE_LOCATION)){
            writer.write(json);
        } catch (FileNotFoundException e) {
            displayError("Etwas ist beim Speichern der Daten fehlgeschlagen.");
        }
    }

    private void loadFromJson() {
        try (final JsonReader reader = new JsonReader(new FileReader(SAVE_FILE_LOCATION))){
            KontaktListe output = gson.fromJson(reader, KontaktListe.class);
            kontakte = output.getListe();
        } catch (IOException e) {
            displayError("Es konnte keine Sicherung gefunden/geladen werden.");
        }
    }

    private String[][] getTableCells() {
        String[][] res = new String[kontakte.size()][6];
        for (int i = 0; i < kontakte.size(); i++) {
            Kontakt current = kontakte.get(i);
            res[i][0] = String.valueOf(current.getKontaktnummer());
            res[i][1] = current.getName();
            res[i][2] = current.getVorname();
            res[i][3] = current.getGeburtsdatum();
            res[i][4] = current.getTelefonnummer();
            res[i][5] = current.getEmail();
        }
        return res;
    }
}