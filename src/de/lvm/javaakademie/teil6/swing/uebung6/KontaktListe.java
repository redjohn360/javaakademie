package de.lvm.javaakademie.teil6.swing.uebung6;

import java.util.ArrayList;
import java.util.List;

public class KontaktListe {
    private List<Kontakt> liste;

    public KontaktListe() {
        liste = new ArrayList<>();
    }

    public KontaktListe(List<Kontakt> liste) {
        this.liste = liste;
    }

    public List<Kontakt> getListe() {
        return liste;
    }
}
