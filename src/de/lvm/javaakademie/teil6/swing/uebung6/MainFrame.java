package de.lvm.javaakademie.teil6.swing.uebung6;

import de.lvm.javaakademie.teil6.swing.uebung6.panels.FormPanel;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class MainFrame extends JFrame {

    private FormPanel formPanel;

    public MainFrame() {
			super("Kontakterfassung");
			Container panel = getContentPane();

			formPanel = new FormPanel();

			KontaktController controller = new KontaktController(this);
			panel.add(formPanel);

			URL imgurl = this.getClass().getResource("icon42x42.png");
			setIconImage(new ImageIcon(imgurl).getImage());

			setSize(400, 400);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setResizable(false);
			setVisible(true);
    }

    public FormPanel getFormPanel() {
			return formPanel;
    }
}
