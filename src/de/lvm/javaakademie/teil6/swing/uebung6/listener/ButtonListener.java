package de.lvm.javaakademie.teil6.swing.uebung6.listener;

public interface ButtonListener {
    void fireEvent();
}
