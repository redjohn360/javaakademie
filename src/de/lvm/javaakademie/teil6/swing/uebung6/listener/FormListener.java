package de.lvm.javaakademie.teil6.swing.uebung6.listener;

import de.lvm.javaakademie.teil6.swing.uebung6.events.FormEvent;

public interface FormListener {
    void eventFired(FormEvent formEvent);
}
