package de.lvm.javaakademie.teil6.swing.uebung6.panels;

import de.lvm.javaakademie.teil6.swing.uebung6.Kontakt;
import de.lvm.javaakademie.teil6.swing.uebung6.events.FormEvent;
import de.lvm.javaakademie.teil6.swing.uebung6.listener.ButtonListener;
import de.lvm.javaakademie.teil6.swing.uebung6.listener.FormListener;

import javax.swing.*;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import java.awt.*;

public class FormPanel extends JPanel {

    private JLabel kontaktnummerLable;
    private JLabel nameLable;
    private JLabel vornameLable;
    private JLabel geburtsTagLable;
    private JLabel geburtsTagLableBack;
    private JLabel telefonnummerLable;
    private JLabel emailLable;
    private JLabel beschreibungLable;

    private JTextField kontaktnummerField;
    private JTextField nameField;
    private JTextField vornameField;
    private JTextField gebutsTagField;
    private JTextField telefonnummerField;
    private JTextField emailField;

    private JTextArea beschreibungField;

    private JCheckBox beschreibungAnnehmen;

    private FormButtonPanel formButtonPanel;

    private FormListener formListener;
    private ButtonListener buttonListener;

    public FormPanel() {
        kontaktnummerLable = new JLabel("Kontaktnr.");
        nameLable = new JLabel("Name");
        vornameLable = new JLabel("Vorname");
        geburtsTagLable = new JLabel("Geburtstag");
        geburtsTagLableBack = new JLabel("TT.MM.JJJJ");
        telefonnummerLable = new JLabel("Telefonnr.");
        emailLable = new JLabel("E-Mail");
        beschreibungLable = new JLabel("Beschreibung");

        kontaktnummerField = new JTextField(10);
        nameField = new JTextField(25);
        vornameField = new JTextField(25);
        gebutsTagField = new JTextField(10);
        telefonnummerField = new JTextField(10);
        emailField = new JTextField(25);

        beschreibungField = new JTextArea(4,25);

        beschreibungAnnehmen = new JCheckBox("Beschreibung übernehmen");

        formButtonPanel = new FormButtonPanel();

        setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        /* Generell */
        gc.fill = GridBagConstraints.NONE;
        gc.anchor = GridBagConstraints.LINE_START;
        gc.insets = new Insets(0, 10,0,0);
        gc.weighty = 1;
        gc.weightx = 1;

        /* Kontakt */
        gc.gridwidth = 1;
        gc.gridx = 0;
        gc.gridy = 0;
        add(kontaktnummerLable, gc);

        gc.gridwidth = 1;
        gc.gridx = 1;
        add(kontaktnummerField, gc);

        /* Name */
        gc.gridwidth = 1;
        gc.gridx = 0;
        gc.gridy++;
        add(nameLable, gc);

        gc.gridwidth = 2;
        gc.gridx = 1;
        add(nameField, gc);

        /* Vorname */
        gc.gridwidth = 1;
        gc.gridx = 0;
        gc.gridy++;
        add(vornameLable, gc);

        gc.gridwidth = 2;
        gc.gridx = 1;
        add(vornameField, gc);

        /* Geburtstag */
        gc.gridwidth = 1;
        gc.gridx = 0;
        gc.gridy++;
        add(geburtsTagLable, gc);

        gc.gridwidth = 1;
        gc.gridx = 1;
        add(gebutsTagField, gc);

        gc.gridwidth = 1;
        gc.gridx = 2;
        add(geburtsTagLableBack, gc);

        /* Telefonnummer */
        gc.gridwidth = 1;
        gc.gridx = 0;
        gc.gridy++;
        add(telefonnummerLable, gc);

        gc.gridwidth = 1;
        gc.gridx = 1;
        add(telefonnummerField, gc);

        /* E-Mail */
        gc.gridwidth = 1;
        gc.gridx = 0;
        gc.gridy++;
        add(emailLable, gc);

        gc.gridwidth = 2;
        gc.gridx = 1;
        add(emailField, gc);

        /* Bescheibung */
        gc.gridwidth = 1;
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        gc.gridx = 0;
        gc.gridy++;
        add(beschreibungLable, gc);

        gc.gridwidth = 2;
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        gc.gridx = 1;
        add(new JScrollPane(beschreibungField), gc);

        /* Checkbox */
        gc.gridwidth = 2;
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        gc.gridx = 1;
        gc.gridy++;
        add(beschreibungAnnehmen, gc);

        /* Buttons */
        gc.gridwidth = 3;
        gc.anchor = GridBagConstraints.CENTER;
        gc.gridx = 0;
        gc.gridy++;
        add(formButtonPanel, gc);

        formButtonPanel.getAnzeigenButton().addActionListener(e -> {
            FormEvent event = new FormEvent();
            event.setKontaktnummer(kontaktnummerField.getText());
            event.setName(nameField.getText());
            event.setVorname(vornameField.getText());
            event.setGeburtsdatum(gebutsTagField.getText());
            event.setTelefonnummer(telefonnummerField.getText());
            event.setEmail(emailField.getText());
            event.setBeschreibung(beschreibungField.getText());
            event.setBeschreibungUebernehmen(beschreibungAnnehmen.isSelected());
            event.setNeuAnlegen(false);
            formListener.eventFired(event);
        });

        formButtonPanel.getAnlegenButton().addActionListener(e -> {
            FormEvent event = new FormEvent();
            event.setKontaktnummer(kontaktnummerField.getText());
            event.setName(nameField.getText());
            event.setVorname(vornameField.getText());
            event.setGeburtsdatum(gebutsTagField.getText());
            event.setTelefonnummer(telefonnummerField.getText());
            event.setEmail(emailField.getText());
            event.setBeschreibung(beschreibungField.getText());
            event.setBeschreibungUebernehmen(beschreibungAnnehmen.isSelected());
            event.setNeuAnlegen(true);
            formListener.eventFired(event);
        });

        formButtonPanel.getTabelleButton().addActionListener(e -> {
            buttonListener.fireEvent();
        });
    }

    public void displayKontakt(Kontakt kontakt) {
        kontaktnummerField.setText(String.valueOf(kontakt.getKontaktnummer()));
        nameField.setText(kontakt.getName());
        vornameField.setText(kontakt.getVorname());
        gebutsTagField.setText(kontakt.getGeburtsdatum());
        telefonnummerField.setText(kontakt.getTelefonnummer());
        emailField.setText(kontakt.getEmail());
        beschreibungField.setText(kontakt.getBeschreibung());
    }

    public void setFormListener(FormListener formListener) {
        this.formListener = formListener;
    }

    public void setButtonListener(ButtonListener buttonListener) {
        this.buttonListener = buttonListener;
    }
}
