package de.lvm.javaakademie.teil6.swing.uebung6.panels;

import javax.swing.*;
import java.awt.*;

public class FormButtonPanel extends JPanel {

    private JButton anzeigenButton;
    private JButton anlegenButton;
    private JButton tabelleButton;

    public FormButtonPanel() {
        anlegenButton = new JButton("Anlegen");
        anzeigenButton = new JButton("Anzeigen");
        tabelleButton = new JButton("Tabelle");

        setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        gc.fill = GridBagConstraints.NONE;
        gc.insets = new Insets(0,2,10,2);

        gc.gridy = 0;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.LINE_END;
        add(anzeigenButton, gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.CENTER;
        add(anlegenButton, gc);

        gc.gridx = 2;
        gc.anchor = GridBagConstraints.LINE_START;
        add(tabelleButton, gc);
    }

    public JButton getAnzeigenButton() {
        return anzeigenButton;
    }

    public JButton getAnlegenButton() {
        return anlegenButton;
    }

    public JButton getTabelleButton() {
        return tabelleButton;
    }
}
