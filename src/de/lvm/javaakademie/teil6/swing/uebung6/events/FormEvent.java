package de.lvm.javaakademie.teil6.swing.uebung6.events;

public class FormEvent {

    private String kontaktnummer;
    private String name;
    private String vorname;
    private String geburtsdatum;
    private String telefonnummer;
    private String email;
    private String beschreibung;
    private boolean beschreibungUebernehmen;
    private boolean neuAnlegen;

    public FormEvent() {
    }

    public String getKontaktnummer() {
        return kontaktnummer;
    }

    public void setKontaktnummer(String kontaktnummer) {
        this.kontaktnummer = kontaktnummer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(String geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public boolean isBeschreibungUebernehmen() {
        return beschreibungUebernehmen;
    }

    public void setBeschreibungUebernehmen(boolean beschreibungUebernehmen) {
        this.beschreibungUebernehmen = beschreibungUebernehmen;
    }

    public boolean isNeuAnlegen() {
        return neuAnlegen;
    }

    public void setNeuAnlegen(boolean neuAnlegen) {
        this.neuAnlegen = neuAnlegen;
    }
}
