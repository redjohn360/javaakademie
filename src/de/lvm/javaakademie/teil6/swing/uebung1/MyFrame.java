package de.lvm.javaakademie.teil6.swing.uebung1;

import javax.swing.*;
import java.awt.*;

public class MyFrame extends JFrame {
	@Override
	public void paint(Graphics g) {
		g.setFont(Font.getFont(Font.SERIF));
		g.drawString("LVM", 200, 200);
	}
}
