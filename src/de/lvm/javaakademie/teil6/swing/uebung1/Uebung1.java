package de.lvm.javaakademie.teil6.swing.uebung1;

import javax.swing.*;
import java.awt.*;

public class Uebung1{

	public static void main(String[] args) {
		JFrame frame = new MyFrame();
		frame.setSize(400, 400);
		frame.setVisible(true);
		frame.setBackground(Color.green.darker());
		frame.setForeground(Color.green.brighter());
	}
}
