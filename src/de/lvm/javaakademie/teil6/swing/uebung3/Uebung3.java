package de.lvm.javaakademie.teil6.swing.uebung3;

import javax.swing.*;

public class Uebung3 extends JPanel{

	private JButton big, s1, s2, s3;

	public static void main(String[] args) {
		JFrame frame = new JFrame("Einfaches Swing Beispiel");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Uebung3 newContentPane = new Uebung3();
		newContentPane.setOpaque(true);
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Exception e) {

		}
		frame.setContentPane(newContentPane);
		frame.pack();
		frame.setVisible(true);
	}

	public Uebung3() {
		big = new JButton("Unser LVM");
		big.setVerticalTextPosition(AbstractButton.CENTER);
		big.setHorizontalAlignment(AbstractButton.CENTER);

		s1 = new JButton("Matal");
		s1.setVerticalTextPosition(AbstractButton.CENTER);
		s1.setHorizontalAlignment(AbstractButton.CENTER);

		s2 = new JButton("Motif");
		s2.setVerticalTextPosition(AbstractButton.CENTER);
		s2.setHorizontalAlignment(AbstractButton.CENTER);

		s3 = new JButton("Windows");
		s3.setVerticalTextPosition(AbstractButton.CENTER);
		s3.setHorizontalAlignment(AbstractButton.CENTER);

		add(big);
		add(s1);
		add(s2);
		add(s3);
	}

}
