package de.lvm.javaakademie.teil6.swing.uebung3;

public class Kontakt {
	private int kontaktnummer;
	private String Name;
	private String Vorname;
	private String Telefon;
	private String EMail;

	public Kontakt(final int kontaktnummer, final String name, final String vorname, final String telefon,
			final String EMail) {
		this.kontaktnummer = kontaktnummer;
		Name = name;
		Vorname = vorname;
		Telefon = telefon;
		this.EMail = EMail;
	}

	public int getKontaktnummer() {
		return kontaktnummer;
	}

	public void setKontaktnummer(final int kontaktnummer) {
		this.kontaktnummer = kontaktnummer;
	}

	public String getName() {
		return Name;
	}

	public void setName(final String name) {
		Name = name;
	}

	public String getVorname() {
		return Vorname;
	}

	public void setVorname(final String vorname) {
		Vorname = vorname;
	}

	public String getTelefon() {
		return Telefon;
	}

	public void setTelefon(final String telefon) {
		Telefon = telefon;
	}

	public String getEMail() {
		return EMail;
	}

	public void setEMail(final String EMail) {
		this.EMail = EMail;
	}
}
