package de.lvm.javaakademie.teil6.hausaufgabe1;

import java.math.BigDecimal;
import utils.logger;

public class main {

	public static void main(String[] args) {
		runden(new BigDecimal("0.13"));
		runden(new BigDecimal("-0.13"));
		runden(new BigDecimal("0.15"));
		runden(new BigDecimal("0.25"));
		runden(new BigDecimal("-0.15"));
		runden(new BigDecimal("0.10"));
	}

	public static void runden(final BigDecimal input) {
		logger.log(input.setScale(1, BigDecimal.ROUND_CEILING).toString());
		logger.log(input.setScale(1, BigDecimal.ROUND_DOWN).toString());
		logger.log(input.setScale(1, BigDecimal.ROUND_FLOOR).toString());
		logger.log(input.setScale(1, BigDecimal.ROUND_HALF_DOWN).toString());
		logger.log(input.setScale(1, BigDecimal.ROUND_HALF_EVEN).toString());
		logger.log(input.setScale(1, BigDecimal.ROUND_HALF_UP).toString());
		try {
			logger.log(input.setScale(1, BigDecimal.ROUND_UNNECESSARY).toString());
		} catch (ArithmeticException e) {
			logger.log("Error");
		}
		logger.log(input.setScale(1, BigDecimal.ROUND_UP).toString());
	}
}
