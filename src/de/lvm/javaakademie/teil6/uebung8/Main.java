package de.lvm.javaakademie.teil6.uebung8;

import java.math.BigDecimal;

import utils.logger;

public class Main {

	public static final int KOMMA_SELLEN = 20;

	public static void main(String[] args) {
		BigDecimal eins = new BigDecimal(50);
		BigDecimal zwei = new BigDecimal(20);
		BigDecimal drei = new BigDecimal(100);
		BigDecimal vier = new BigDecimal(0.1);
		BigDecimal fuenf = new BigDecimal(0.000001);
		BigDecimal sechs = new BigDecimal(1000);

		logger.log(Main.prozent(eins, zwei).toString());
		logger.log(Main.prozent(drei, drei).toString());
		logger.log(Main.prozent(drei, vier).toString());
		logger.log(Main.prozent(fuenf, vier).toString());
	}

	public static BigDecimal prozent (BigDecimal argBetrag, BigDecimal argProzentsatz) {
		BigDecimal eins = argBetrag.divide(new BigDecimal(100)).multiply(argProzentsatz);
		eins = eins.setScale(KOMMA_SELLEN, BigDecimal.ROUND_HALF_UP);
		return eins;
	}
}
