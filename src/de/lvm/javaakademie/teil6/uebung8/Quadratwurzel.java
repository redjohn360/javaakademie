package de.lvm.javaakademie.teil6.uebung8;

import java.math.BigDecimal;

public class Quadratwurzel {

	public static void main(String[] args) {
		Quadratwurzel quadratwurzel = new Quadratwurzel();
		System.out.println(quadratwurzel.quadratwurzel(new BigDecimal(2), 4000));
	}

	public BigDecimal quadratwurzel(BigDecimal argX, int argGenauigkeit) {
		BigDecimal oben = argX;
		BigDecimal unten = new BigDecimal("0");
		BigDecimal ersteNaeherung = oben.subtract(unten).divide(new BigDecimal("2"));
		BigDecimal kontrollWert = ersteNaeherung.multiply(ersteNaeherung);
		BigDecimal fehler = argX.subtract(kontrollWert).abs();

		BigDecimal genau = new BigDecimal("1").movePointLeft(argGenauigkeit + 1);

		while (fehler.compareTo(genau) > 0) {
			if (kontrollWert.compareTo(argX) > 0) {
				oben = ersteNaeherung;
			} else {
				unten = ersteNaeherung;
			}
			ersteNaeherung = oben.add(unten).divide(new BigDecimal("2"));
			kontrollWert = ersteNaeherung.multiply(ersteNaeherung);
			fehler = argX.subtract(kontrollWert).abs();
		}
		return ersteNaeherung.setScale(argGenauigkeit, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
	}
}
