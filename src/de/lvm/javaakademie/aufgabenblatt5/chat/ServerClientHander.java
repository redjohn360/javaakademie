package de.lvm.javaakademie.aufgabenblatt5.chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

public class ServerClientHander {

	private final DataInputStream dataInputStream;

	private final DataOutputStream dataOutputStream;

	private Future<?> process;

	private String name;

	private boolean active = true;

	public ServerClientHander(DataInputStream dataInputStream, DataOutputStream dataOutputStream) {
		this.dataInputStream = dataInputStream;
		this.dataOutputStream = dataOutputStream;

		setUp();
	}

	public void setUp() {
		sendeBefehl("?SCREENNAME?");
		System.out.println("Client wird dazu aufgefordert, seinen Namen zu senden.");
		ChatHelper.addSimpleCommandListener(this::meldeAn, dataInputStream);
	}

	private void meldeAn(final String name) {
		if (name.contains(" ") && Server.einzigartig(name)) {
			setUp();
		} else {
			System.out.println("Der Client hat folgenden Namen gesendet: " + name);
			this.name = name;
			sendeBefehl("!NAMEACCEPTED!");

			Map<String, EventHandler> handlerMap = new HashMap<>();
			handlerMap.put(":QUIT:", this::quit);
			handlerMap.put("", this::nachrichtErhalten);

			process = ChatHelper.addCommandListener(handlerMap, dataInputStream);
		}
	}

	private void quit(final String nachricht) {
		process.cancel(true);
		System.out.println("Der Nutzer " + name + " hat erfolgreich die Verbindung getrennt.");
		Server.verteileNachricht("Der Nutzer " + name + " hat erfolgreich die Verbindung getrennt.", this);
		try {
			dataInputStream.close();
			dataOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void nachrichtErhalten(final String nachricht) {
		System.out.println("Nachricht erhalten.");
		Server.verteileNachricht(name + ": " + nachricht, this);
	}

	public void sendeNachricht(final String nachricht) {
		sendeBefehl("!MESSAGE!" + nachricht);
	}

	private void sendeBefehl(final String befehl) {
		ChatHelper.sendeBefehl(befehl, dataOutputStream);
	}

	public boolean isActive() {
		return active;
	}
}
