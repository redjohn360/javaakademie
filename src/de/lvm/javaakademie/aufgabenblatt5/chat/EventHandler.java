package de.lvm.javaakademie.aufgabenblatt5.chat;

public interface EventHandler {
    void run(final String input);
}
