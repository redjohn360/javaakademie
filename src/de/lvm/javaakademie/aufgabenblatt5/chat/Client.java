package de.lvm.javaakademie.aufgabenblatt5.chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Future;

public class Client {

    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private Future<?> process;
    private final Scanner scanner;
    private String name;

    public static void main(String[] args) {
        Client client = new Client();
    }

    public Client() {
        scanner = new Scanner(System.in);
        System.out.print("Bitte geben sie die IP-Adresse des Servers ein: ");
        try {
            Socket socket = new Socket(scanner.nextLine(), 9080);
            this.dataInputStream = new DataInputStream(socket.getInputStream());
            this.dataOutputStream = new DataOutputStream(socket.getOutputStream());
            setUp();
        } catch (IOException ignored) {
        }
    }

    public void setUp() {
        Map<String, EventHandler> handlerMap = new HashMap<>();
        handlerMap.put("?SCREENNAME?", this::sendeName);
        handlerMap.put("!NAMEACCEPTED!", this::accepted);

        process = ChatHelper.addCommandListener(handlerMap, dataInputStream);
    }

    private void accepted(final String message) {
    	System.out.println("Sie sind erfolgreich dem Chat beigetreten.");
        System.out.println(process.cancel(true));
        System.out.println(process.isCancelled());

        Map<String, EventHandler> handlerMap = new HashMap<>();
        handlerMap.put("!MESSAGE!", this::printMessage);

        process = ChatHelper.addCommandListener(handlerMap, dataInputStream);
        input();
    }

    private void sendeName(final String message) {
        System.out.print("Bitte gib deinen Namen ein: ");
        name = scanner.nextLine();
        sendeBefehl(name);
    }

    private void printMessage(final String message) {
        String[] split = message.split("!MESSAGE!");
        if(message.startsWith(name + ":")) return;
        System.out.println(split[1]);
    }

    private void input() {
        while (true) {
        	System.out.print("Du: ");
            String s = scanner.nextLine();
            sendeBefehl(s);
            if(s.equals(":QUIT:")) {
							process.cancel(true);
							System.out.println("Verbindung zum Server getrennt.");
							System.exit(1);
            }
        }
    }

    private void sendeBefehl(final String befehl) {
        ChatHelper.sendeBefehl(befehl, dataOutputStream);
    }
}
