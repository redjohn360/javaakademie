package de.lvm.javaakademie.aufgabenblatt5.chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

    private static List<ServerClientHander> serverClientHanders;

    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }

    public Server() {
        serverClientHanders = new ArrayList<>();
    }

    public void start() {
        try {
            ServerSocket serverSocket = new ServerSocket(9080);
            while (true) {
                final Socket socket = serverSocket.accept();
                System.out.println("Ein Client mit der IP " + socket.getInetAddress().getHostAddress() +
										"hat sich mit dem Server Verbunden");
                final DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                final DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                serverClientHanders.add(new ServerClientHander(dataInputStream, dataOutputStream));
            }
        } catch (IOException ignored) {
        }
    }

    public static void verteileNachricht(final String nachricht, final ServerClientHander serverClientHander) {
        serverClientHanders.forEach(client -> {
        	if(!client.equals(serverClientHander)) {
						client.sendeNachricht(nachricht);
					}
				});
        serverClientHanders.removeIf(e -> !e.isActive());
    }

    public static boolean einzigartig(final String name) {
        return true;
    }
}
