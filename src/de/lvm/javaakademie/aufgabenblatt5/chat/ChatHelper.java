package de.lvm.javaakademie.aufgabenblatt5.chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ChatHelper {

    private static ExecutorService pool = Executors.newCachedThreadPool();


    public static void sendeBefehl(final String befehl, final DataOutputStream dataOutputStream) {
        try {
            dataOutputStream.writeUTF(befehl);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Future<?> addSimpleCommandListener(EventHandler eventHandler, final DataInputStream dataInputStream) {
        return pool.submit(() -> {
            try {
                final String input = dataInputStream.readUTF();
                eventHandler.run(input);
            } catch (IOException ignored) {
            }
        });
    }

    public static Future<?> addCommandListener(Map<String, EventHandler> commands, final DataInputStream dataInputStream) {
        return pool.submit(() -> {
           while (!Thread.currentThread().isInterrupted()) {
               try {
                   final String input = dataInputStream.readUTF();
                   commands.forEach((string, command) -> {
                       if (input.startsWith(string)) command.run(input);
                   });
               } catch (IOException e) {
                   return;
               }
           }
        });
    }
}
