package de.lvm.javaakademie.aufgabenblatt5.intervall;

import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {
        Intervall intervall = new Intervall(new BigInteger("10"), new BigInteger("20"));
        Intervall intervall2 = new Intervall(new BigInteger("9"), new BigInteger("22"));
        System.out.println(intervall.ueberschneiden(intervall2));
        System.out.println(intervall2.ueberschneiden(intervall));
        Intervall intervall3 = intervall.zusammenfasssen(intervall2);
        System.out.println(intervall3.toString());
    }
}
