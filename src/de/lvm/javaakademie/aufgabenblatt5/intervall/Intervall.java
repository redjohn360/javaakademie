package de.lvm.javaakademie.aufgabenblatt5.intervall;

import java.math.BigInteger;

public class Intervall {

    private final BigInteger oben;
    private final BigInteger unten;

    public Intervall(BigInteger unten, BigInteger oben) {
        System.out.println(oben + ":" + unten);
        if(unten.compareTo(oben) > 0) {
            throw new IllegalArgumentException();
        }
        this.oben = oben;
        this.unten = unten;
    }

    public int length() {
        return Integer.parseInt(oben.subtract(unten).toString());
    }

    public boolean ueberschneiden(final Intervall intervall){
        return unten.compareTo(intervall.getOben()) <= 0 && oben.compareTo(intervall.getUnten()) >= 0;
    }

    public boolean zusammenfassbar(final Intervall intervall) {
        return ueberschneiden(intervall);
    }

    public Intervall zusammenfasssen(final Intervall intervall) {
        return zusammenfassbar(intervall) ? new Intervall(unten.min(intervall.getUnten()), oben.max(intervall.getOben())) : null;
    }

    public BigInteger getOben() {
        return oben;
    }

    public BigInteger getUnten() {
        return unten;
    }
}
