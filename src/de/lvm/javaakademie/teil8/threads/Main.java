package de.lvm.javaakademie.teil8.threads;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<Integer> collection = new ArrayList<>();

		ZufallszahlThread[] threads = new ZufallszahlThread[5];
		for (int i = 0; i < threads.length; i++) {
			threads[i] = new ZufallszahlThread(collection);
		}

		Arrays.stream(threads).forEach(thread -> {
			thread.start();
		});

		Arrays.stream(threads).forEach(thread -> {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});

		System.out.println(collection.size()+"LAPPEN");

	}
}
