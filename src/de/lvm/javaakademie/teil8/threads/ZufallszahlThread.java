package de.lvm.javaakademie.teil8.threads;

import java.util.List;

public class ZufallszahlThread extends Thread {

	private int zahl;
	private List<Integer> collection;

	@Override
	public void run() {
		zahl = (int) Math.floor(Math.random() * 100);
		synchronized (collection) {
			collection.add(zahl);
		}
	}

	public ZufallszahlThread(final List<Integer> collection) {
		this.collection = collection;
	}

	public int getZahl() {
		return zahl;
	}
}
