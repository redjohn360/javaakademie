package de.lvm.javaakademie.teil8.netzwerke.uebung1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	private static final int SERVER_PORT = 9090;

	public static void main(String[] args) {
		try (ServerSocket listener = new ServerSocket(SERVER_PORT)) {
			while (true) {
				System.out.println("Warte auf Verbindung...");

				Socket socket = listener.accept();
				System.out.println("Verbunden...");

				DataInputStream input = new DataInputStream(socket.getInputStream());

				final int eins = input.readInt();
				System.out.println("Input gelesen: " + eins);

				final int zwei = input.readInt();
				System.out.println("Input gelesen: " + zwei);

				DataOutputStream output = new DataOutputStream(socket.getOutputStream());
				output.writeInt(eins + zwei);
				System.out.println("Ergebnis \"" + eins+zwei + "\" gesendet.");
				socket.close();
				System.out.println("Verbindung geschlossen.");
			}
		} catch(IOException e){
			e.printStackTrace();
		}
	}
}
