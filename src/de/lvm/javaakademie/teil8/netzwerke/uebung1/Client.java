package de.lvm.javaakademie.teil8.netzwerke.uebung1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client {

	public static void main(String[] args) {
		try (Socket socket = new Socket("localhost", 9090)){
			System.out.println("Verbindung zum Server " + socket.getInetAddress().getHostAddress() + " hergestellt.");

			DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
			outputStream.writeInt(2);
			System.out.println("Scheibe Zahl 1: \"2\"");

			outputStream.writeInt(4);
			System.out.println("Scheibe Zahl 1: \"4\"");

			DataInputStream inputStream = new DataInputStream(socket.getInputStream());
			System.out.println("Ergebnis \"" +inputStream.readInt() + "\" vom Server bekommen.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
