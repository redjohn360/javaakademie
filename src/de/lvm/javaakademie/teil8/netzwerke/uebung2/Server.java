package de.lvm.javaakademie.teil8.netzwerke.uebung2;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

	private static final int SERVER_PORT = 9090;

	public static void main(String[] args) {
		try (ServerSocket listener = new ServerSocket(SERVER_PORT)) {
			ExecutorService pool = Executors.newCachedThreadPool();
			while (true) {
				System.out.println("Warte auf Verbindung...");
				pool.execute(new ClientHander(listener.accept()));
			}
		} catch(IOException e){
			e.printStackTrace();
		}
	}
}
