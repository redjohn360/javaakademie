package de.lvm.javaakademie.teil8.netzwerke.uebung2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientHander implements Runnable{

	final Socket socket;

	@Override
	public void run() {
		try {
			DataInputStream input = new DataInputStream(socket.getInputStream());

			final int eins = input.readInt();
			System.out.println("Input gelesen: " + eins);

			final int zwei = input.readInt();
			System.out.println("Input gelesen: " + zwei);

			DataOutputStream output = new DataOutputStream(socket.getOutputStream());
			output.writeInt(eins + zwei);
			System.out.println("Ergebnis \"" + eins + zwei + "\" gesendet.");
			socket.close();
			System.out.println("Verbindung geschlossen.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ClientHander(final Socket socket) {
		this.socket = socket;
		System.out.println("Verbunden...");
	}
}
