package de.lvm.javaakademie.reflection.uebung1;

import java.lang.reflect.Field;
import java.util.Arrays;

public class ClassExplorer {

	private static String klassenName;

	public static void main (String args[]) {
		if(args.length == 1) {
			try {
				Vergleicher vgl = new Vergleicher();
				klassenName = args[0];
				Class cl = Class.forName(klassenName);
				Field[] fl = cl.getDeclaredFields();
				Arrays.sort(fl, vgl);
				System.out.println("Felder:");
				for (Field field :fl) {
					System.out.println(field /*field.getType().getSimpleName() + " " + field.getName()*/);
				}
			} catch (ClassNotFoundException e) {

			}
		}
	}
}
