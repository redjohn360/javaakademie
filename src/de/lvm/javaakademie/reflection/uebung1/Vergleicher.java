package de.lvm.javaakademie.reflection.uebung1;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Comparator;

public class Vergleicher implements Comparator<Field>{
	@Override
	public int compare(final Field o1, final Field o2) {
		int res = 0;
		res = teste(Modifier.isStatic(o1.getModifiers()), Modifier.isStatic(o1.getModifiers()));
		if (res == 0) {
			res = teste(Modifier.isPublic(o1.getModifiers()), Modifier.isPublic(o1.getModifiers()));
		}
		return 0;
	}

	@Override
	public boolean equals(final Object obj) {
		return false;
	}

	public int teste(boolean o1, boolean o2) {
		if (o1 && o2) {
			return 0;
		} else if(o1) {
			return -1;
		} else if(o2) {
			return 1;
		}
		return 0;
	}
}
