package de.lvm.javaakademie.teil4.uebung1;

public class Preis implements Besuchbar {

	private final int preisSommer;
	private final int preisWinter;

	public Preis(final int preisSommer, final int preisWinter) {
		this.preisSommer = preisSommer;
		this.preisWinter = preisWinter;
	}

	public void acceptVisitor(Visitor visitor) {
		visitor.visitPreis(this);
	}

	public int getPreisSommer() {
		return preisSommer;
	}

	public int getPreisWinter() {
		return preisWinter;
	}

}
