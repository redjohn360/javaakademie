package de.lvm.javaakademie.teil4.uebung1;

public class VisitorZaehler implements Visitor{

	private int gesamtPreis;
	private boolean summer;

	public VisitorZaehler(final boolean summer) {
		this.summer = summer;
	}

	@Override
	public void visitReise(Reise reise) {
		System.out.print(reise.getType() + ": " + reise.getName() + " ");
		reise.getPreis().acceptVisitor(this);
	}

	@Override
	public void visitPreis(Preis preis ) {
		System.out.println(summer ? preis.getPreisSommer() : preis.getPreisWinter()  + " Euro");
		gesamtPreis += summer ? preis.getPreisSommer() : preis.getPreisWinter();
	}

	@Override
	public int getGesamtPreis() {
		return gesamtPreis;
	}
}
