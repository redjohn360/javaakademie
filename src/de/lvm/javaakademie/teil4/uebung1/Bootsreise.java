package de.lvm.javaakademie.teil4.uebung1;

public class Bootsreise extends Reise {

	public Bootsreise(final String name, final Preis preis) {
		super(name, preis);
	}

	public String getType() {
		return "Bootsreise";
	}
}
