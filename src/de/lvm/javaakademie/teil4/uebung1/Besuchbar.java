package de.lvm.javaakademie.teil4.uebung1;

public interface Besuchbar {
	public void acceptVisitor(Visitor visitor);
}
