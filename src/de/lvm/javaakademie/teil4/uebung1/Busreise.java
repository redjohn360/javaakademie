package de.lvm.javaakademie.teil4.uebung1;

public class Busreise extends Reise {

	public Busreise(final String name, final Preis preis) {
		super(name, preis);
	}

	@Override
	public String getType() {
		return "Busreise";
	}
}
