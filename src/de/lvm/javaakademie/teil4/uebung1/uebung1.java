package de.lvm.javaakademie.teil4.uebung1;

public class uebung1 {
	public static void main(String[] args) {
		final Preis p1 = new Preis(1000, 10);
		final Preis p2 = new Preis(50, 5);

		final Reise br1 = new Busreise("Mallorca", p1);
		final Reise br2 = new Busreise("London", p1);
		final Reise br3 = new Bootsreise("Südsee", p2);
		final Visitor visitorZaehler = new VisitorZaehler(false);

		br1.acceptVisitor(visitorZaehler);
		br2.acceptVisitor(visitorZaehler);
		br3.acceptVisitor(visitorZaehler);
		System.out.println(visitorZaehler.getGesamtPreis());
	}
}
