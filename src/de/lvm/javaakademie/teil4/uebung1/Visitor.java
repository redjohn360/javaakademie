package de.lvm.javaakademie.teil4.uebung1;

public interface Visitor {

	public void visitReise(Reise reise);

	public void visitPreis(Preis preis);

	public int getGesamtPreis();
}
