package de.lvm.javaakademie.teil4.uebung1;

public abstract class Reise implements Besuchbar {

	private final String name;
	private final Preis preis;

	public Reise(final String name, final Preis preis) {
		this.name = name;
		this.preis = preis;
	}

	public Preis getPreis() {
		return preis;
	}

	@Override
	public void acceptVisitor(Visitor visitor) {
		visitor.visitReise(this);
	}

	public String getName() {
		return name;
	}

	public abstract String getType();

}
