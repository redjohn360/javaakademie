package de.lvm.javaakademie.teil3.aufgabe2;

public class Rechteck extends Polygon {

    private int seite1 = 0;
    private int seite2 = 0;

    public Rechteck(int seite1, int seite2, int hoehe) {
        super(hoehe);
        this.seite1 = seite1;
        this.seite2 = seite2;
    }

    public int flaecheBerechnen() {
        return seite1*seite2;
    }
}
