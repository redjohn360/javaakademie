package de.lvm.javaakademie.teil3.aufgabe2;

public abstract class Polygon {

    private int hoehe = 0;

    public Polygon(int hoehe) {
        this.hoehe = hoehe;
    }

    public abstract int flaecheBerechnen();

    public int volumenBerechnen () {
        return flaecheBerechnen()*hoehe;
    }
}
