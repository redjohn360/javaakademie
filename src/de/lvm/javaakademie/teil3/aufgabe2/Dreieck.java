package de.lvm.javaakademie.teil3.aufgabe2;

public class Dreieck extends Polygon {

    public int grundflaeche = 0;
    public int hoehe = 0;

    public Dreieck(int grundflaeche, int hoehe, int hoehe3d) {
        super(hoehe3d);
        this.grundflaeche = grundflaeche;
        this.hoehe = hoehe;
    }

    public int flaecheBerechnen () {
        return (grundflaeche*hoehe)/2;
    }
}
