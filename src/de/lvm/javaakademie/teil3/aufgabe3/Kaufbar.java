package de.lvm.javaakademie.teil3.aufgabe3;

public interface Kaufbar {
    public int getPreis();
}
