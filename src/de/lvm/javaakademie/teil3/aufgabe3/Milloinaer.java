package de.lvm.javaakademie.teil3.aufgabe3;

public class Milloinaer {

    private int vermoegen;

    public Milloinaer(int vermoegen) {
        this.vermoegen = vermoegen;
    }

    public void kaufeSachen(Kaufbar kaufbar) {
        vermoegen -= kaufbar.getPreis();
        System.out.println(vermoegen);
    }
}
