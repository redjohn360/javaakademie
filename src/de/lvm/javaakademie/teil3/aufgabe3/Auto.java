package de.lvm.javaakademie.teil3.aufgabe3;

public class Auto implements Kaufbar {
    private final int preis;

    public Auto(int preis) {
        this.preis = preis;
    }

    public int getPreis() {
        return preis;
    }
}
