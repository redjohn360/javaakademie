package de.lvm.javaakademie.teil3.aufgabe3;

public class Weltreise implements Kaufbar {
     private final int preis;

    public Weltreise(int preis) {
        this.preis = preis;
    }

    public int getPreis() {
        return preis;
    }
}
