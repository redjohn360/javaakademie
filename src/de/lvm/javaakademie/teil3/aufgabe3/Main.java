package de.lvm.javaakademie.teil3.aufgabe3;

public class Main {
    public static void main(String[] args) {
        Auto auto = new Auto(1000);
        Milloinaer milloinaer = new Milloinaer(1000000);
        milloinaer.kaufeSachen(auto);

        Weltreise weltreise = new Weltreise(100000);
        milloinaer.kaufeSachen(weltreise);
    }
}
