package de.lvm.javaakademie.teil3.aufgabe7;

public class aufgabe7 {

    public static void main(String[] args) {
        Aktie aktie = new Aktie();
        Textanzeige textanzeige = new Textanzeige();
        Textanzeige textanzeige1 = new Textanzeige();


        aktie.addObserver(textanzeige);
        aktie.addObserver(textanzeige1);
        aktie.changeText("Hallo");
    }
}
