package de.lvm.javaakademie.teil3.aufgabe7;

import java.util.ArrayList;

public class Aktie implements Observable{
   final ArrayList<Observer> observers = new ArrayList<>();
   private String text = "Hallo";

   public void addObserver(Observer o) {
       observers.add(o);
   }

   public void removeObserver(Observer o) {
       observers.remove(o);
   }

   public String getText() {
       return text;
   }

   public void onChange() {
       for (Observer o: observers) {
           o.update(this);
       }
   }

   public void changeText(String text) {
       this.text = text;
       onChange();
   }
}
