package de.lvm.javaakademie.teil3.aufgabe7;

public interface Observable {
    public void addObserver(Observer o);
    public void removeObserver(Observer o);

    public String getText();
}
