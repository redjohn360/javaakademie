package de.lvm.javaakademie.teil3.aufgabe7;

public class Textanzeige implements Observer{

    public void update(Observable o) {
        System.out.println(o.getText());
    }
}
