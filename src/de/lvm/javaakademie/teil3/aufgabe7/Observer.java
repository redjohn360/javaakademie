package de.lvm.javaakademie.teil3.aufgabe7;

public interface Observer {
    public void update(Observable o);
}
