package de.lvm.javaakademie.teil3.aufgabe4;

public interface LohnsteuerRechner {
    public double berechneNetto(double brutto);
}
