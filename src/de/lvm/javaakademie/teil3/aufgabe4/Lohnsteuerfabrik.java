package de.lvm.javaakademie.teil3.aufgabe4;

public class Lohnsteuerfabrik {
    public static LohnsteuerRechner getLohnsteuerRechner(int jahr) {
        switch (jahr){
            case 2017:
                return new LohnsteuerRechner2017();
            case 2016:
                return new LohnsteuerRechner2016();
            default:
                return null;
        }
    }
}
