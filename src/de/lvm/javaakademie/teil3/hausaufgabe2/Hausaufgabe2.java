package de.lvm.javaakademie.teil3.hausaufgabe2;

import de.lvm.javaakademie.teil3.aufgabe6.LogginSingelton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Klasse für die Hausaufgabe 2 der Java Akademie
 *
 * @author m500092 (Luca Büning)
 */
public class Hausaufgabe2 {
    private static LogginSingelton logger;

    public static void main(String[] args) {
        Hausaufgabe2 hausaufgabe = new Hausaufgabe2();
        /*
            2.1 Quersumme
         */
        //logger.log(String.valueOf(hausaufgabe.sumOfTheDigits(1534)));

        /*
            2.2 Morsecode
         */
        //logger.log(hausaufgabe.convertToMorse("SOS"));

        /*
            2.3 Römische Zahlen
         */
        logger.log(String.valueOf(hausaufgabe.roman2decimal("IIXVM")));
        logger.log(hausaufgabe.decimal2Roman(2314));
    }

    private Hausaufgabe2(){
        logger = LogginSingelton.getInstance();
    }

    /**
     * Berechnet die Quersumme von Longs
     *
     * @param value Zahl von der die Quersumme berechnet werden soll
     * @return Quersummme
     */
    private int sumOfTheDigits(long value) {
        return sumOfTheDigits(String.valueOf(value));
    }

    /**
     * Berechnet die Quersumme von Strings
     *
     * @param value Zahl von der die Quersumme berechnet werden soll
     * @return Quersummme
     */
    private int sumOfTheDigits(String value) {
        int sum = 0;
        for (int i = 0; i < value.length(); i++) {
            sum += Integer.parseInt(Character.toString(value.charAt(i)));
        }
        return sum;
    }

    /**
     * Aufgabe 1.2: Konvertiert einen String in Mosecode
     *
     * @param input String der zu Konvertieren ist
     * @return morsecode als String
     */
    private String convertToMorse(String input) {
        final String[] alphabet = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8",
                "9", "0", " " };

        final String[] morseCode = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
                "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.",
                "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-",
                "-.--", "--..", ".----", "..---", "...--", "....-", ".....",
                "-....", "--...", "---..", "----.", "-----", "|" };

        String finalCode = "";
        for (int i = 0; i < input.length(); i++) {
            for (int j = 0; j < alphabet.length; j++) {
                if(Character.toString(input.charAt(i)).equalsIgnoreCase(alphabet[j])) {
                    finalCode += morseCode[j] + " ";
                }
            }
        }
        return finalCode;
    }

    /**
     * Konvertiert einen String von einer römischen Zahl in eine Decimal Zahl
     *
     * @param input
     * @return Zahl als Dezimalzahl
     */
    private int roman2decimal(String input) {
        int[] digits = convertArr2Decimal(input);
        int sum = 0;
        for (int i = 0; i < digits.length; i++) {
            if (i != digits.length-1 && digits[i+1] > digits[i]) {
                sum -= digits[i];
            } else {
                sum += digits[i];
            }
        }
        return sum;
    }

    /**
     * Zerstückelt eine römische Zahl in die einzelnen Teilzahlen
     *
     * @param input
     * @return
     */
    private int[] convertArr2Decimal (final String input) {
        final int[] result = new int[input.length()];
        final String romanChiphers = "IVXLCDM";
        final int[] romanChipherNumbers = {1, 5, 10, 50, 100, 500, 1000};
        for (int i = 0; i < input.length(); i++) {
            result[i] = romanChipherNumbers[romanChiphers.indexOf(input.charAt(i))];
        }
        return result;
    }

    private String decimal2Roman (int input) {
        final String romanChiphers = "IVXLCDM";
        final int[] romanChipherNumbers = {1, 5, 10, 50, 100, 500, 1000};

        int lauf = romanChipherNumbers.length -1;
        String output = "";
        while (input != 0) {
            if (input / romanChipherNumbers[lauf] >= 1) {
                input -= romanChipherNumbers[lauf];
                output += romanChiphers.charAt(lauf);
            } else {
                lauf--;
            }
        }
        return output;
    }

}