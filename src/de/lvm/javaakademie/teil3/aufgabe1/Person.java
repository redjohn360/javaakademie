package de.lvm.javaakademie.teil3.aufgabe1;

public class Person {

    private int budget;

    public Person (int budget) {
        this.budget = budget;
    }

    public void kaufe (Fahrzeug f) {
        if (f.getPreis() <= budget) {
            budget -= f.getPreis();
            System.out.println(f.getClass().getSimpleName() + " wurde gekauft! ");
        } else {
            System.out.println("Ne einfach ne");
        }
    }
}
