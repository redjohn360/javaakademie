package de.lvm.javaakademie.teil3.aufgabe1;

public abstract class Fahrzeug {

    protected final int preis;

    public Fahrzeug (int preis) {
        this.preis = preis;
    }

    public int getPreis () {
        return preis;
    }

}
