package de.lvm.javaakademie.teil3.aufgabe1;

public class Main {

    public static void main (String[] args) {
        Person person = new Person(10000);
        Auto auto = new Auto(100);
        Motorad motorad = new Motorad(1000);
        person.kaufe(motorad);
    }
}
