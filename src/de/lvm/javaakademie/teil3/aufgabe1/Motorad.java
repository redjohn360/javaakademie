package de.lvm.javaakademie.teil3.aufgabe1;

public class Motorad extends Fahrzeug {

    public Motorad (int preis) {
        super(preis);
    }

    @Override
    public int getPreis() {
        return preis;
    }
}
