package de.lvm.javaakademie.teil3.aufgabe1;

public class Auto extends Fahrzeug{

    public Auto (int preis) {
        super(preis);
    }

    @Override
    public int getPreis() {
        return preis;
    }
}
