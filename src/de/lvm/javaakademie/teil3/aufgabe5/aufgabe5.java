package de.lvm.javaakademie.teil3.aufgabe5;

public class aufgabe5 {

    public static void main(String[] arg) {
        HatName[] hatNames = {new Person("Lars", "Elsbrösel"), new Haustier("Hundi")};
        for (HatName hatName: hatNames) {
            System.out.println(hatName.getName());
        }
    }
}
