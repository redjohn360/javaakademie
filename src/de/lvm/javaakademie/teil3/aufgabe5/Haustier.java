package de.lvm.javaakademie.teil3.aufgabe5;

public class Haustier implements HatName {

    private final String name;

    public Haustier(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
