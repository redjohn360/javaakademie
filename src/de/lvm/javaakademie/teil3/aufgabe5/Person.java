package de.lvm.javaakademie.teil3.aufgabe5;

public class Person implements HatName{
    private final String vorname;
    private final String nachname;

    public Person(String vorname, String nachname) {
        this.vorname = vorname;
        this.nachname = nachname;
    }

    public String getName() {
        return vorname + " " + nachname;
    }
}
