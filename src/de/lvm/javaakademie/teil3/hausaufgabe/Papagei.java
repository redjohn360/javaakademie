package de.lvm.javaakademie.teil3.hausaufgabe;

public class Papagei extends Haustier {

    public Papagei(int gewicht, String kosename) {
        super(gewicht, kosename);
    }

    public void gibLaut () {
        System.out.println(kosename);
    }
}
