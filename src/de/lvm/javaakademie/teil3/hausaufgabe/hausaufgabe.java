package de.lvm.javaakademie.teil3.hausaufgabe;

public class hausaufgabe {

    public static void main (String[] args) {
        Haustier[] haustiere = {new Fisch(1, "Goldi"), new Hund(25, "Bello"),
        new Papagei(5, "Alfons")};

        for (Haustier haustier: haustiere) {
            haustier.gibLaut();
        }
    }
}
