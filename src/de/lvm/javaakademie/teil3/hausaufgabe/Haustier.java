package de.lvm.javaakademie.teil3.hausaufgabe;

public abstract class Haustier {

    protected final int gewicht;
    protected final String kosename;

    public Haustier(int gewicht, String kosename) {
        this.gewicht = gewicht;
        this.kosename = kosename;
    }

    public abstract void gibLaut();
}
