package de.lvm.javaakademie.teil3.hausaufgabe;

public class Fisch extends Haustier {

    public Fisch(int gewicht, String kosename) {
        super(gewicht, kosename);
    }

    public void gibLaut () {
        System.out.println("Blub");
    }
}
