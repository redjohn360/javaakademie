package de.lvm.javaakademie.teil3.hausaufgabe;

public class Hund extends Haustier {

    public Hund(int gewicht, String kosename) {
        super(gewicht, kosename);
    }

    public void gibLaut () {
        for (int i = 0; i < gewicht ; i++) {
            System.out.println("Wau");
        }
    }
}
