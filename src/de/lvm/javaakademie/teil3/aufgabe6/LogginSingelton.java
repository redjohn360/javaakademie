package de.lvm.javaakademie.teil3.aufgabe6;

public class LogginSingelton {
    private static LogginSingelton instance;

    public static LogginSingelton getInstance() {
        if (instance == null) {
            instance = new LogginSingelton();
        }
        return instance;
    }

    public void log (String text) {
        System.out.println(text);
    }
}
