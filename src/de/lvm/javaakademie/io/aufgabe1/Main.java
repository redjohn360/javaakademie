package de.lvm.javaakademie.io.aufgabe1;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] arg) {
		File test = new File("/home/m500092/Schreibtisch");
		System.out.println(Main.printFile(test, ""));
	}

	public static String printFile(File file, String tabs) {
		if (file.isFile()) {
			return tabs + file.getName() + "\n";
		} else  {
			File[] files = file.listFiles();
			String res = "";
			try {
				res = tabs + file.getCanonicalPath() + "\n";
				for (File fileInArray: files) {
					res += Main.printFile(fileInArray, tabs + "\t");
				}
			} catch (IOException e) {

			} catch (NullPointerException e) {
				return tabs + file.getName() + "\n";
			}
			return res;
		}
	}
}
