package de.lvm.javaakademie.aufgabenblatt3.schaltjahr;

import java.util.Calendar;

public class schaltjahr {

	public static void main(String[] args) {
		System.out.println(isSchaltjahr(2008));
	}

	public static boolean isSchaltjahr(final int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		return calendar.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
	}
}
