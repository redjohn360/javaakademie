package de.lvm.javaakademie.aufgabenblatt3.schneckenrennen;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<Rennschnecke> schnegge = new ArrayList<>();
		schnegge.add(new Rennschnecke("Luca", "Toll", 12));
		schnegge.add(new Rennschnecke("Simon", "Ok", 2));
		//Rennen rennen = new Rennen("Normal", )
		//schnegge.forEach(schnegg -> System.out.println(schnegg));
		schnegge.forEach(schnegg -> schnegg.krieche());
		schnegge.forEach(schnegg -> System.out.println(schnegg));
	}
}
