package de.lvm.javaakademie.aufgabenblatt3.schneckenrennen;

import java.util.List;

public class Rennen {

	private final String name;
	private final int anzahl;
	private final List<Rennschnecke> teilnehmer;
	private final double strecke;

	public Rennen(final String name, final List<Rennschnecke> teilnehmer, final double strecke) {
		this.name = name;
		this.anzahl = teilnehmer.size();
		this.teilnehmer = teilnehmer;
		this.strecke = strecke;
	}


}
