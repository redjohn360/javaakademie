package de.lvm.javaakademie.aufgabenblatt3.schneckenrennen;

public class Rennschnecke {

	final String name;
	final String rasse;
	final double maximalGeschwindigkeit;
	double weg = 0;

	public Rennschnecke(final String name, final String rasse, final double maximalGeschwindigkeit) {
		this.name = name;
		this.rasse = rasse;
		this.maximalGeschwindigkeit = maximalGeschwindigkeit;
	}

	public void krieche() {
		weg += Math.random()*maximalGeschwindigkeit;
	}

	public String toString() {
		return name + ", " + rasse + " |" + weg;
	}

}
