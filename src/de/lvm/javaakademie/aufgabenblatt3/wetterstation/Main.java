package de.lvm.javaakademie.aufgabenblatt3.wetterstation;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import de.lvm.javaakademie.teil6.uebung5.HashMapUebung;

public class Main {

	public static void main(String[] args) {
		Main main = new Main();
		System.out.println(main.durchschnitt());
		main.minMax();
		main.greatesDiff();
		main.print();
	}

	HashMap<Integer, Integer> tabelle;

	public Main() {
		tabelle = new HashMap<>();
		Integer[] werte = new Integer[]{12, 14,9,12,15,16,15,15,11,8,13,15,12};
		for (int i = 1; i <= werte.length; i++) {
			tabelle.put(i, werte[i-1]);
		}
	}

	public int durchschnitt() {
		int gesamt = 0;

		for (Map.Entry<Integer, Integer> wert : tabelle.entrySet()) {
			gesamt+=wert.getValue();
		}
		return gesamt/tabelle.size();
	}

	public void minMax() {
		int min = Collections.min(tabelle.values());
		int max = Collections.max(tabelle.values());
		System.out.println("Der Maximale Wert ist: " + max + "\nDer Minimale Wert ist: " + min);
	}

	public void greatesDiff() {
		int tag1 = 0;
		int tag2 = 0;
		int diff = 0;
		for (int i = 1; i < tabelle.size(); i++) {
			final int currentDiff = Math.abs(tabelle.get(i) - tabelle.get(i+1));
			if(diff < currentDiff) {
				diff = currentDiff;
				tag1 = i;
				tag2 = i+1;
			}
		}
		System.out.println("Der größte Unterschie war von Tag " + tag1 + " auf Tag " + tag2 +  " mit " + diff + " Grad differenz.");
	}

	public void print() {
		String res = "___Tag______Wert__\n";
		for (Map.Entry<Integer, Integer> feld : tabelle.entrySet()) {
			res += "||\t" + feld.getKey() + "\t||\t" + feld.getValue() + "\t||\n";
		}
		res += "__________________\n";
		System.out.println(res);
	}


}
