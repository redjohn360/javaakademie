package de.lvm.javaakademie.teil2.uebung1;

/**
 * Mit einem Lenker kann man das Auto lenken
 */
public class Lenker {
	
	private String material;

	/**
	 * Der Lenker lenkt das Auto
	 * @param pMaterial Das Material aus dem der Lenker gemacht ist
	 */
	public Lenker(String pMaterial) {
		material = pMaterial;
	}
	
	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}
}
