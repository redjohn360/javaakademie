package de.lvm.javaakademie.teil2.uebung1;

public class Main {
	
	public static void main (String[] args) {
		Auto autoEins = new Auto("Leder", 9999);
		Auto autoZwei = new Auto("Kein Leder", 10);

		Person person1 = new Person(10000);
		Person person2 = new Person(50000);

		for (int i = 0; i < 9; i++) {
			person1.kaufeAuto(autoEins);
		}

		while (person2.istBazahlbar2(autoZwei)){
			person2.kaufeAuto(autoZwei);
		}
	}
}