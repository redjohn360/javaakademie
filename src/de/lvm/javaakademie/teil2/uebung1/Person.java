package de.lvm.javaakademie.teil2.uebung1;

public class Person {
    private int budget;

    public Person (int budget) {
        this.budget = budget;
    }

    public void kaufeAuto (Auto auto) {
        if (auto.getPreis() > budget) {
            System.out.println("Zu teuer Guthaben: " + budget);
        } else {
            budget -= auto.getPreis();
            System.out.println("Gekauft neues Guthaben: " + budget);
        }
    }

    public boolean istBezahlbar (Auto auto) {
        if (auto.getPreis() > budget) {
            return false;
        } else {
            return true;
        }
    }

    public boolean istBazahlbar2 (Auto auto) {
        return auto.getPreis() > budget ? false : true;
    }
}
