package de.lvm.javaakademie.teil2.uebung1;

public class Autoradio {

    private int i = 0;
    private final float[] frequenzen = {107.7f , 43.7f, 45.9f, 231.9f, 48.3f};

    public Autoradio () {

    }

    public final void nextFrequenz () {
        if (i >= frequenzen.length - 1){
            i = 0;
        } else {
            i++;
        }
        System.out.println(frequenzen[i]);
    }

    public final void lastFrequenz () {
        if (i <= 0){
            i = 4;
        } else {
            i--;
        }
        System.out.println(frequenzen[i]);
    }

    public final float aktuelleFrequenz () {
        return frequenzen[i];
    }

    public float[] getFrequenzen() {
        return frequenzen;
    }
}
