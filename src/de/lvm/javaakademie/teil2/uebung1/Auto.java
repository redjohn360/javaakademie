package de.lvm.javaakademie.teil2.uebung1;

/**
 * Das ist ein Auto, es hat Räder und kann fahren
 */
public class Auto {

	private int geschwindingkeit = 0;
	private int insassen = 0;
	private Lenker lenker;
	private Autoradio autoradio;
	private int preis;

	/**
	 * Erstellt ein neues Auto
	 * @param pLenkerMaterial Das Material des Lenkers
	 */
	public Auto (String pLenkerMaterial, int preis) {
		lenker = new Lenker(pLenkerMaterial);
		autoradio = new Autoradio();
		this.preis = preis;
	}

	/**
	 * Eine Person steigt in das Auto ein
	 */
	public void Einsteigen () {
		insassen++;
	}

	/**
	 * Erhöht die Geschwindigkeit
	 */
	public void GasTreten () {
		geschwindingkeit = geschwindingkeit + 10;
	}

	/**
	 * Lässt das Auto verunfallen
	 */
	public void Unfall () {
		insassen = 0;
		geschwindingkeit = 0;
	}

	/**
	 * Gibt die aktuelle Geschwindigkeit und die Insassen in der Konsole aus
	 */
	public void toConsole () {
		System.out.println("Gescheindigkeit: " + geschwindingkeit + " Insassen: " + insassen + "Autoradiofrequenz: " + autoradio.aktuelleFrequenz());
	}
	
	public String getLenkrad () {
		return lenker.getMaterial();
	}

	public int getGeschwindingkeit() {
		return geschwindingkeit;
	}

	public void setGeschwindingkeit(int geschwindingkeit) {
		this.geschwindingkeit = geschwindingkeit;
	}

	public int getPreis() {
		return preis;
	}

	public void setPreis(int preis) {
		this.preis = preis;
	}

	public int getInsassen() {
		return insassen;
	}

	public void setInsassen(int insassen) {
		this.insassen = insassen;
	}
}