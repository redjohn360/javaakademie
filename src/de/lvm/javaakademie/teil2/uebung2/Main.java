package de.lvm.javaakademie.teil2.uebung2;

public class Main {

    public static boolean istTrue = true;
    public static boolean istFalse = false;

    public static void main (String[] args) {
        System.out.println(String.valueOf(istFalse == istTrue) + " FALSE");
        System.out.println(String.valueOf(istFalse == istFalse) + " TRUE");
        System.out.println(String.valueOf(istFalse != istTrue) + " TRUE");
        System.out.println(String.valueOf(istFalse && istTrue) + " FALSE");
        System.out.println(String.valueOf(istFalse || istTrue) + " TRUE");
        boolean test = !istTrue ? true : true;
    }
}
