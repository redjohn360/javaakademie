package de.lvm.javaakademie.teil2.hausaufgabe;

import java.lang.reflect.Array;
import java.util.Arrays;

public class hausaufgabe {

    public static void main (String[] args) {
        /*
         * Aufgabe 1
         *
         * Wenn die Zahlen zu groß sind passen sie nicht mehr in den Double
         */
        System.out.println(avg(new double[]{100000,500000}, 2));

        /*
         * Aufgabe 2
         */
        //reverse(new double[]{1,2,3,4});

        /*
         * Aufgabe 3
         */
        /*int[] intArray = toInts(new String[]{"342", "3546", "393021"});
        for (int zahl: intArray) {
            System.out.println(zahl);
        }*/

        /*
         * Aufgabe 4
         */
        //System.out.println(vokalaustausch("Drei Chinesen mit dem Kontrabass...", 'a'));
    }

    /**
     * Aufgabe 1
     *
     * @param array
     * @param len
     * @return
     */
    public static double avg (double[] array, int len) {
        double sum = 0.0;
        for (int i = 0; i < len; i++) {
            sum += array[i];
        }
        return sum/len;
    }

    /**
     * Aufgabe 2
     *
     * @param array
     */
    public static void reverse(double[] array) {
        if(array == null) {
            return;
        }
        for (double ziffer: array) {
            System.out.println(ziffer);
        }
        for (int i = 0; i < array.length/2; i++) {
            final double first = array[i];
            array[i] = array[array.length-i-1];
            array[(array.length-i)-1] = first;
        }
        for (double ziffer: array) {
            System.out.println(ziffer);
        }
    }

    /**
     * Aufgabe 3
     *
     * @param array
     * @return
     */
    public static int[] toInts (String[] array) {
        int[] intArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            intArray[i] = Integer.parseInt(array[i]);
        }
        return intArray;
    }

    /**
     * Aufgabe 4
     *
     * @param s
     * @param c
     * @return
     */
    public static String vokalaustausch(String s, char c) {
        String[] langeVokale = {"ei", "au", "Ei", "Au", "a", "e", "i", "o", "u"};
        for (String langerVokal: langeVokale) {
            s = s.replace(langerVokal, String.valueOf(c));
        }
        return s;
    }
}
