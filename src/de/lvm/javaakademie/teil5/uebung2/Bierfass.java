package de.lvm.javaakademie.teil5.uebung2;

public class Bierfass {

	private int liter;

	public Bierfass(final int liter) {
		this.liter = liter;
	}

	public void bierTrinken() throws BierLeerExecption {
		if (liter == 0) {
			throw new BierLeerExecption("Schnell das Fass ist leer!");
		} else {
			liter--;
		}
	}
}
