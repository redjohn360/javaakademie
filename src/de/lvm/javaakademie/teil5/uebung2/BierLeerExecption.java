package de.lvm.javaakademie.teil5.uebung2;

public class BierLeerExecption extends Exception {
	public BierLeerExecption(String message) {
		super(message);
	}
}
