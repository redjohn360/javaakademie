package de.lvm.javaakademie.teil5.uebung2;

import de.lvm.javaakademie.teil3.aufgabe6.LogginSingelton;

public class uebung2 {

	public static void main(String[] args) {
		final Bierfass bierfass = new Bierfass(0);
		final LogginSingelton logger = new LogginSingelton();
		try {
			bierfass.bierTrinken();
		} catch (BierLeerExecption e) {
			logger.log(e.getMessage());
		}
	}
}
