package de.lvm.javaakademie.teil5.uebung1;

import java.util.StringTokenizer;

public class uebung1 {
		public static void main (String[] args) {
			String string = "Dies ist eine Übungsaufgabe, die nicht besonders schwierig ist. Also: Los geht's! ";
			StringTokenizer s = new StringTokenizer(string," :,;.!");
			while (s.hasMoreElements()) {
				System.out.println(s.nextElement());
			}
		}
}
