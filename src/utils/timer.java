package utils;

public class timer {
	private static long start;

	public static void start() {
		start = System.currentTimeMillis();
	}

	public static void stop() {
		System.out.println("Zeit: " + (System.currentTimeMillis() - start));
	}
}
